# Makefile - makefile for rexima

CC=gcc
CFLAGS=-O -Wall

# Set BINDIR to directory for binary,
# MANDIR to directory for man page.
# Usually it will be simpler to just set PREFIX.
#
PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/man/man1

# You shouldn't need to edit below this line.
#--------------------------------------------------------

all: rexima

rexima: rexima.o
	$(CC) $(CFLAGS) -o rexima rexima.o -lncurses

installdirs:
	/bin/sh ./mkinstalldirs $(BINDIR) $(MANDIR)

install: rexima installdirs
	install -s -m 755 rexima $(BINDIR)
	install -m 644 rexima.1 $(MANDIR)

uninstall:
	$(RM) $(BINDIR)/rexima $(MANDIR)/rexima.1

clean:
	$(RM) *.o *~ rexima


# stuff to make distribution tgz

VERS=1.4

tgz: ../rexima-$(VERS).tar.gz
  
../rexima-$(VERS).tar.gz: clean
	@cd ..;ln -s rexima rexima-$(VERS)
	cd ..;tar zchvf rexima-$(VERS).tar.gz rexima-$(VERS)
	@cd ..;$(RM) rexima-$(VERS)
